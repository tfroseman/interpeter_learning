package lexer

import (
	"interpeter_learning/token"
)

type Lexer struct {
	input string // Code input
	position int // Current position
	readPosition int // Next position in the input
	char byte  // Current Char value
}

/*
	Param: input | Typeof String
	Return: Lexer | Typeof Pointer

	Return a point to a lexer created with the input
	and read the fist char
 */
func New(input string) *Lexer {
	lexer := &Lexer{input: input}
	lexer.readChar()
	return lexer
}

/*
	Param: lexer | Typeof Pointer
	Returns: Token | Typeof struct(Token)

	Return a fully inflated Token based on that the
	current char in the lexer is.
 */
func (lexer *Lexer) NextToken() token.Token {
	var assignedToken token.Token

	lexer.skipWhiteSpace()

	switch lexer.char {
	case '=':
		if lexer.peekChar() == '='{
			assignedToken = token.Token{Type: token.EQUAL, Literal: lexer.doubleToken()}
		} else {
			assignedToken = newToken(token.ASSIGN, lexer.char)
		}
	case ';':
		assignedToken = newToken(token.SEMICOLON, lexer.char)
	case '(':
		assignedToken = newToken(token.LEFT_PARENTHESES, lexer.char)
	case ')':
		assignedToken = newToken(token.RIGHT_PARENTHESES, lexer.char)
	case ',':
		assignedToken = newToken(token.COMMA, lexer.char)
	case '+':
		assignedToken = newToken(token.PLUS, lexer.char)
	case '{':
		assignedToken = newToken(token.LEFT_BRACE, lexer.char)
	case '}':
		assignedToken = newToken(token.RIGHT_BRACE, lexer.char)
	case '!':
		if lexer.peekChar() == '=' {
			assignedToken = token.Token{Type: token.NOT_EQUAL, Literal: lexer.doubleToken()}
		} else {
			assignedToken = newToken(token.BANG, lexer.char)
		}
	case '<':
		assignedToken = newToken(token.LT, lexer.char)
	case '>':
		assignedToken = newToken(token.GT, lexer.char)
	case '-':
		assignedToken = newToken(token.MINUS, lexer.char)
	case '*':
		assignedToken = newToken(token.ASTERISK, lexer.char)
	case '/':
		assignedToken = newToken(token.SLASH, lexer.char)
	case 0:
		assignedToken.Literal = ""
		assignedToken.Type = token.EOF
	default:
		if isLetter(lexer.char) {
			assignedToken.Literal = lexer.readIdentifier()
			assignedToken.Type = token.LookupIdent(assignedToken.Literal)
			return assignedToken
		} else if isDigit(lexer.char){
			assignedToken.Type = token.INT
			assignedToken.Literal = lexer.readNumber()
			return assignedToken
		} else {
			assignedToken = newToken(token.ILLEGAL, lexer.char)
		}

	}

	// Advance the lexer read head to the next char
	lexer.readChar()

	return assignedToken
}

func (lexer *Lexer) readIdentifier() string {
	position := lexer.position
	for isLetter(lexer.char){
		lexer.readChar()
	}
	return lexer.input[position: lexer.position]
}

func isLetter(char byte) bool {
	return 'a' <= char && char <= 'z' || 'A' <= char && char <= 'Z' || char == '_'
}

/**
	Param: tokenType | Typeof token.TokenType, char | Typeof Byte
	Returns: Token | Typeof struct(Token)

	Return the inflated token with a the tokenType being a const string
	and the char being the current byte from the input string
 */
func newToken(tokenType token.TokenType, char byte) token.Token {
	return token.Token{Type: tokenType, Literal: string(char)}
}

/**
	Param: lexer | Typeof Pointer to Lexer

	Advance the lexer read head and lookahead read
 */
func (lexer *Lexer) readChar(){
	if lexer.readPosition >= len(lexer.input) {
		lexer.char = 0
	} else {
		lexer.char = lexer.input[lexer.readPosition]
	}
	lexer.position = lexer.readPosition
	lexer.readPosition += 1
}

func (lexer *Lexer) skipWhiteSpace() {
	for lexer.char == ' ' || lexer.char == '\t' || lexer.char == '\n' || lexer.char == '\r' {
		lexer.readChar()
	}
}

func (lexer *Lexer) readNumber() string {
	position := lexer.position
	for isDigit(lexer.char) {
		lexer.readChar()
	}
	return lexer.input[position:lexer.position]
}

func isDigit(char byte) bool {
	return '0' <= char && char <= '9'
}

func (lexer *Lexer) peekChar() byte  {
	if lexer.readPosition >= len(lexer.input) {
		return 0
	} else {
		return lexer.input[lexer.readPosition]
	}
}

func (lexer *Lexer) doubleToken() string{
	char := lexer.char
	lexer.readChar()
	return string(char) + string(lexer.char)
}